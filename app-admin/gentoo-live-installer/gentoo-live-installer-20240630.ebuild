# Copyright 1999-2024 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=8

COMMIT_ID="a9325b2659f80504c277ef654c9d1081fbfc4561"
DESCRIPTION="Gentoo Live Installer"
HOMEPAGE="https://gitlab.com/ms1888/gentoo-live-installer"
SRC_URI="https://gitlab.com/ms1888/gentoo-live-installer/-/archive/${COMMIT_ID}.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL-2.1 LGPL-3"
SLOT="0"
KEYWORDS="amd64 x86"
S=${WORKDIR}/${PN}-${COMMIT_ID}
RESTRICT="mirror"

RDEPEND="
	dev-python/pillow
	dev-python/pygobject:3
	dev-python/pyparted
	x11-apps/setxkbmap"

src_install() {
	mv ${S}/etc "${ED}" || die
	mv ${S}/usr "${ED}" || die
}
