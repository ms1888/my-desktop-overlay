# Copyright 2020-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PYTHON_COMPAT=( python3_{10,11,12} )

inherit distutils-r1 git-r3

DESCRIPTION="Solus os-installer - Gentoo port"
HOMEPAGE="https://getsol.us"
EGIT_REPO_URI="https://gitlab.com/ms1888/os-installer"

LICENSE="GPL-2 CC-BY-SA-4.0"
SLOT="0"
KEYWORDS=""
IUSE=""

RDEPEND="
	dev-libs/libtimezonemap
	dev-python/pygobject:3
	dev-python/pyparted[${PYTHON_USEDEP}]
	gnome-base/gnome-desktop:3"
