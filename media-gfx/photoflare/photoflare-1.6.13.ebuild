# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit qmake-utils xdg

DESCRIPTION="Quick, simple but powerful Cross Platform image editor"
HOMEPAGE="https://github.com/PhotoFlare/photoflare"
SRC_URI="https://github.com/PhotoFlare/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

RDEPEND="dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtwidgets:5
	dev-qt/qtprintsupport:5
	media-gfx/graphicsmagick:=
	sys-libs/libomp"
DEPEND="${RDEPEND}"
BDEPEND="dev-qt/linguist-tools:5"

src_configure() {
	eqmake5 PREFIX="${EPREFIX}"/usr
}

src_install() {
	einstalldocs
	emake INSTALL_ROOT="${ED}" install
}
