# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

ECM_TEST="forceoptional"
PVCUT=$(ver_cut 1-3)
KFMIN=5.67.0
QTMIN=5.15.0
inherit ecm kde.org

if [[ ${KDE_BUILD_TYPE} == release ]] ; then
	SRC_URI="mirror://kde/unstable/${PN}/${P}.tar.xz"
	KEYWORDS="amd64"
fi

DESCRIPTION="Simple and user-friendly Jabber/XMPP client for every device and platform"
HOMEPAGE="https://www.kaidan.im"

LICENSE="GPL-3+ CC-BY-SA-4.0"
SLOT="0"
IUSE="kde"

DEPEND="
	dev-libs/kirigami-addons:5
	>=dev-qt/qtcore-${QTMIN}:5
	>=dev-qt/qtconcurrent-${QTMIN}:5
	>=dev-qt/qtdeclarative-${QTMIN}:5
	>=dev-qt/qtlocation-${QTMIN}:5
	>=dev-qt/qtmultimedia-${QTMIN}:5
	>=dev-qt/qtnetwork-${QTMIN}:5
	>=dev-qt/qtpositioning-${QTMIN}:5[qml]
	>=dev-qt/qtquickcontrols-${QTMIN}:5
	>=dev-qt/qtsql-${QTMIN}:5
	>=dev-qt/qtsvg-${QTMIN}:5
	>=dev-qt/qtwidgets-${QTMIN}:5
	>=dev-qt/qtxml-${QTMIN}:5
	>=kde-frameworks/kcoreaddons-${KFMIN}:5
	>=kde-frameworks/kio-${KFMIN}:5
	>=kde-frameworks/kirigami-${KFMIN}:5
	>=kde-frameworks/qqc2-desktop-style-${KFMIN}:5
	media-libs/kquickimageeditor:5
	media-libs/zxing-cpp
	net-libs/qxmpp[omemo]
	kde? ( >=kde-frameworks/knotifications-${KFMIN}:5 )
"
RDEPEND="${DEPEND}"

src_configure() {
	local mycmakeargs=(
		-DUSE_KNOTIFICATIONS=$(usex kde)

		# compile QML at build time
		-DQUICK_COMPILER=ON
	)

	ecm_src_configure
}
