# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake xdg
DESCRIPTION="Draco desktop environment"
HOMEPAGE="https://www.dracolinux.org/"
SRC_URI="https://github.com/rodlie/${PN}/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2 BSD LGPL-2.1"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND="dev-qt/qtcore:5
	dev-qt/qtconcurrent:5
	dev-qt/qtwidgets:5
	dev-qt/qtx11extras:5
	dev-qt/qtgui:5
	dev-qt/qtdbus:5
	x11-libs/libXScrnSaver
	x11-libs/libXdamage
	x11-libs/libXfixes
	x11-libs/libXrandr
	x11-libs/libxcb:0
	x11-libs/xcb-util
	x11-libs/xcb-util-image
	x11-libs/xcb-util-wm"
RDEPEND="${DEPEND}
	sys-power/upower
	sys-fs/udisks:2
	x11-misc/qt5ct
	x11-misc/xdg-utils
	x11-misc/xscreensaver
	x11-wm/openbox"

src_prepare() {
	cmake_src_prepare
}
