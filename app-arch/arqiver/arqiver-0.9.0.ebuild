# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PLOCALES="eo fr hu it nl pl pt_BR ru uk"
inherit plocale qmake-utils xdg

DESCRIPTION="Simple Qt archive manager based on libarchive"
HOMEPAGE="https://github.com/tsujan/Arqiver"
SRC_URI="https://github.com/tsujan/${PN}/archive/V${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

RDEPEND="dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtsvg:5
	dev-qt/qtwidgets:5"
DEPEND="${RDEPEND}"
BDEPEND="dev-qt/linguist-tools"

S=${WORKDIR}/Arqiver-${PV}

src_configure() {
	eqmake5
}

src_install() {
	emake INSTALL_ROOT="${ED}" install
	einstalldocs

	remove_locale() {
		rm -f "${ED}"/usr/share/${PN}/translations/${PN}_${1}.qm
	}
	plocale_for_each_disabled_locale_do remove_locale
}
