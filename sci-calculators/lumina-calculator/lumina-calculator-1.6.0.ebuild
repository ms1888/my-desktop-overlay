# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit qmake-utils xdg

DESCRIPTION="Calculator Utility from the Lumina Desktop"
HOMEPAGE="https://github.com/lumina-desktop/lumina-calculator"
SRC_URI="https://github.com/lumina-desktop/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

RDEPEND="dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtwidgets:5"
DEPEND="${RDEPEND}"
BDEPEND="dev-qt/linguist-tools:5"

S=${WORKDIR}/${P}/src-qt5

src_prepare() {
	default
	sed -i 's@manpage\.extra="\$\${MAN_ZIP}\(.*\)\.gz"@manpage.extra="cat\1"@' src-qt5.pro || die
}

src_configure() {
	eqmake5 \
		L_BINDIR="${EPREFIX}"/usr/bin \
		L_SHAREDIR="${EPREFIX}"/usr/share \
		L_MANDIR="${EPREFIX}"/usr/share/man
}

src_install() {
	einstalldocs
	emake INSTALL_ROOT="${ED}" install
}
