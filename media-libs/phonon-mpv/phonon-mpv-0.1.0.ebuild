# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit cmake kde.org

DESCRIPTION="MPV backend for the Phonon multimedia library"
HOMEPAGE="https://community.kde.org/Phonon"
SRC_URI="https://github.com/OpenProgger/${PN}/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="LGPL-2.1+"
SLOT="0"
KEYWORDS="amd64"
IUSE="qt5 qt6"

DEPEND="
	media-libs/phonon[qt5?,qt6?]
	media-video/mpv:=[libmpv]
	qt5? ( dev-qt/qtx11extras:5 )
	qt6? ( dev-qt/qtbase:6=[X] )
"
RDEPEND="${DEPEND}"
BDEPEND="
	virtual/pkgconfig
"

src_configure() {
	local mycmakeargs=(
		-DKDE_INSTALL_USE_QT_SYS_PATHS=ON
		-DPHONON_BUILD_QT5=$(usex qt5)
		-DPHONON_BUILD_QT6=$(usex qt6)
	)

	cmake_src_configure
}
