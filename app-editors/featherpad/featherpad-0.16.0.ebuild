# Copyright 1999-2022 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

PLOCALES="ar_DZ cs cy da de el eo es fa fr he hu id it ja ko lt nb_NO nl pl pt_BR pt_PT ru sk_SK tr zh_CN zh_TW"
inherit plocale qmake-utils xdg-utils

MY_P="${P/featherpad/FeatherPad}"

DESCRIPTION="Lightweight Qt5 Plain-Text Editor for Linux"
HOMEPAGE="https://github.com/tsujan/FeatherPad"
SRC_URI="https://github.com/tsujan/${PN}/archive/V${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="X"

RDEPEND="dev-qt/qtcore:5
	dev-qt/qtgui:5
	dev-qt/qtwidgets:5
	X? ( dev-qt/qtx11extras:5 )
	dev-qt/qtnetwork:5
	dev-qt/qtprintsupport:5
	dev-qt/qtsvg:5
	app-text/hunspell"
DEPEND="${RDEPEND}"
BDEPEND="dev-qt/linguist-tools:5"

S="${WORKDIR}/${MY_P}"

src_configure() {
	eqmake5 WITHOUT_X11="$(usex X NO YES)"
}

src_install() {
	einstalldocs
	emake INSTALL_ROOT="${ED}" install

	remove_locale() {
		rm -f "${ED}"/usr/share/${PN}/translations/featherpad_${1}.ts
		rm -f "${ED}"/usr/share/${PN}/help_${1}
	}
	plocale_for_each_disabled_locale_do remove_locale
}

pkg_postinst() {
	xdg_desktop_database_update
	xdg_icon_cache_update
	xdg_mimeinfo_database_update
}

pkg_postrm() {
	xdg_desktop_database_update
	xdg_icon_cache_update
	xdg_mimeinfo_database_update
}
