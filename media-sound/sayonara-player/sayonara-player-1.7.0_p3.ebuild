# Copyright 1999-2022 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit cmake xdg

MY_PV=${PV/_p/-stable}
DESCRIPTION="Small, clear and fast audio player"
HOMEPAGE="https://sayonara-player.com/"
SRC_URI="https://sayonara-player.com/files/source/${MY_PV}/${PN}-${MY_PV}.tar.gz"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""
RESTRICT="mirror"

RDEPEND="
	dev-libs/glib:2
	dev-qt/qtcore:5
	dev-qt/qtwidgets:5
	dev-qt/qtdbus:5
	dev-qt/qtnetwork:5
	dev-qt/qtsql:5[sqlite]
	dev-qt/qtxml:5
	dev-qt/linguist-tools:5
	media-libs/gstreamer:1.0
	media-libs/gst-plugins-base:1.0
	media-libs/taglib
	sys-libs/zlib:="
DEPEND="${RDEPEND}"
BDEPEND="virtual/pkgconfig"

S=${WORKDIR}/${PN}-${MY_PV}

src_prepare() {
	xdg_environment_reset
	cmake_src_prepare
}
