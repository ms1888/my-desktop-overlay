# Copyright 2019-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{9..12} )
inherit python-any-r1

DESCRIPTION="LightDM Web Greeter"
HOMEPAGE="https://github.com/JezerM/web-greeter"
SRC_URI="https://github.com/JezerM/web-greeter/archive/${PV}.tar.gz -> ${P}.tar.gz
	https://github.com/JezerM/web-greeter-themes/archive/1.0.0.tar.gz -> web-greeter-themes-1.0.0.tar.gz
	https://github.com/JezerM/nody-greeter-types/archive/4bf0196380.tar.gz -> nody-greeter-types-1.1.0.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86"
RESTRICT="mirror"

RDEPEND="${PYTHON_DEPS}
	$(python_gen_any_dep '
		dev-python/pyqt5[${PYTHON_USEDEP}]
		dev-python/pyqtwebengine[${PYTHON_USEDEP}]
		dev-python/pygobject[${PYTHON_USEDEP}]
		dev-python/pyinotify[${PYTHON_USEDEP}]
		dev-python/ruamel-yaml[${PYTHON_USEDEP}]
	')
	x11-misc/lightdm[introspection,non-root]"
BDEPEND="dev-lang/typescript"

src_prepare() {
	mv ${WORKDIR}/web-greeter-themes*/* ${S}/themes || die
	mv ${WORKDIR}/nody-greeter-types*/* ${S}/themes/ts-types || die
	default
}

src_compile() {
	emake -j1
}

src_install() {
	default
	mv "${ED}"/usr/share/doc/${PN} "${ED}"/usr/share/doc/${P} || die
	rm "${ED}"/usr/bin/web-greeter || die
	dobin "${FILESDIR}"/web-greeter
}
