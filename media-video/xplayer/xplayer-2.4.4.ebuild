# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{11,12} )
PYTHON_REQ_USE="threads(+)"

inherit autotools gnome2-utils vala xdg python-single-r1

DESCRIPTION="Media player for Cinnamon"
HOMEPAGE="https://wiki.gnome.org/Apps/Videos"
SRC_URI="https://github.com/linuxmint/xplayer/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="GPL-2+ LGPL-2+"
SLOT="0"
IUSE="cdr grilo gtk-doc +introspection lirc +python test vala"
# see bug #359379
REQUIRED_USE="
	python? ( introspection ${PYTHON_REQUIRED_USE} )
	vala? ( introspection )
"

KEYWORDS="amd64 ~arm ~arm64 ~ia64 ~ppc ~ppc64 x86"

# FIXME:
# Runtime dependency on gnome-session-2.91
DEPEND="
	>=dev-libs/glib-2.33.0:2
	>=x11-libs/gtk+-3.5.2:3[X,introspection?]
	>=media-libs/gstreamer-1.6.0:1.0
	>=media-libs/gst-plugins-base-1.6.0:1.0[X,pango]
	>=media-libs/gst-plugins-good-1.6.0:1.0
	>=dev-libs/libpeas-1.1.0[gtk]
	dev-libs/totem-pl-parser:=[introspection?]
	>=media-libs/clutter-1.10.0:1.0[gtk]
	>=media-libs/clutter-gst-2.99.2:3.0
	>=media-libs/clutter-gtk-1.0.2:1.0
	gnome-base/gnome-desktop:3=
	gnome-base/gsettings-desktop-schemas

	x11-libs/libICE
	x11-libs/libSM
	x11-libs/libX11
	>=x11-libs/cairo-1.14
	x11-libs/gdk-pixbuf:2
	x11-libs/xapp
	introspection? ( >=dev-libs/gobject-introspection-1.54:= )

	cdr? ( >=dev-libs/libxml2-2.6:2 )
	grilo? ( >=media-libs/grilo-0.3.0:0.3 )
	lirc? ( app-misc/lirc )
	python? (
		${PYTHON_DEPS}
		$(python_gen_cond_dep '
			>=dev-python/pygobject-2.90.3:3[${PYTHON_USEDEP}]
		')
	)
"
RDEPEND="${DEPEND}
	media-plugins/gst-plugins-meta:1.0
	media-plugins/gst-plugins-taglib:1.0
	x11-themes/adwaita-icon-theme
	grilo? ( media-plugins/grilo-plugins:0.3 )
	python? (
		>=dev-libs/libpeas-1.1.0[python,${PYTHON_SINGLE_USEDEP}]
		$(python_gen_cond_dep '
			dev-python/dbus-python[${PYTHON_USEDEP}]
		')
	)
"
BDEPEND="
	dev-lang/perl
	app-text/docbook-xml-dtd:4.5
	app-text/yelp-tools
	dev-util/glib-utils
	>=sys-devel/gettext-0.19.8
	virtual/pkgconfig
	x11-base/xorg-proto
	gtk-doc? (
		>=dev-util/gtk-doc-1.14
		>=dev-util/gtk-doc-am-1.14
	)
	vala? ( $(vala_depend) )
"
# perl for pod2man
# docbook-xml-dtd is needed for user doc
# Prevent dev-python/pylint dep, bug #482538

pkg_setup() {
	use python && python-single-r1_pkg_setup
}

src_prepare() {
	sed -i -e 's/ check-pylint//' src/plugins/Makefile.plugins || die
	sed -i -e '/AC_PATH_PROG.*PYLINT/d' configure.ac || die
	sed -i -e 's/xplayer\(.plparser\)/totem\1/' configure.ac data/xplayer.pc.in || die
	sed -i -e 's/XPLAYER\(.PLPARSER\)/TOTEM\1/' configure.ac data/xplayer.pc.in || die
	sed -i -e 's/xplayer\(.pl.p\)/totem\1/' src/*.? || die
	sed -i -e 's/XPLAYER\(.PL.P\)/TOTEM\1/' src/*.? || die
	sed -i -e 's/Xplayer\(PlP\)/Totem\1/' src/*.? src/Makefile.am || die

	eautoreconf
	use vala && vala_src_prepare
	xdg_src_prepare

	sed -i -e "s|\(gst10_inspect=\).*|\1$(type -P true)|" configure || die
}

src_configure() {
	# Disabled: samplepython, sample-vala, zeitgeist-dp
	# brasero-disc-recorder and gromit require gtk+[X], but totem itself does
	# for now still too, so no point in optionality based on that yet.
	local plugins="apple-trailers,autoload-subtitles"
	plugins+=",im-status,gromit,media-player-keys,ontop"
	plugins+=",properties,recent,screensaver,screenshot"
	plugins+=",skipto,vimeo"
	use cdr && plugins+=",brasero-disc-recorder,chapters"
	use grilo && plugins+=",grilo"
	use lirc && plugins+=",lirc"
	use python && plugins+=",dbusservice,pythonconsole,opensubtitles"
	use vala && plugins+=",rotation"

	local econfargs=(
		--disable-Werror
		--enable-easy-codec-installation
		$(use_enable python)
		$(use_enable vala)
		--with-plugins=${plugins}
		$(use_enable gtk-doc)
		$(use_enable introspection)
	)
	econf "${econfargs[@]}"
}

src_install() {
	emake install DESTDIR="${D}"
	if use python ; then
		python_optimize "${ED}"/usr/$(get_libdir)/totem/plugins/
	fi
}

pkg_postinst() {
	xdg_pkg_postinst
	gnome2_schemas_update
}

pkg_postrm() {
	xdg_pkg_postrm
	gnome2_schemas_update
}
