# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit meson
if [[ ${PV} == 9999 ]] ; then
	inherit git-r3
	EGIT_REPO_URI="https://git.enlightenment.org/apps/${PN}.git"
else
	SRC_URI="http://download.enlightenment.org/rel/apps/${PN}/${P/_/-}.tar.xz"
	KEYWORDS="~amd64 ~x86"
fi

DESCRIPTION="This is a Video + Audio player mplayer style, based on EFL"
HOMEPAGE="https://www.enlightenment.org/about-rage"

LICENSE="BSD-2"
SLOT="0"

DEPEND=">=dev-libs/efl-1.22.3"
RDEPEND="${DEPEND}
	dev-libs/efl[gstreamer]"
BDEPEND="virtual/pkgconfig"

S="${WORKDIR}/${P/_/-}"
