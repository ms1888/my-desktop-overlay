# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

ECM_HANDBOOK="optional"
KFMIN=6.0.0
QTMIN=6.6.0
inherit ecm

DESCRIPTION="Video player built with Qt/QML and libmpv"
HOMEPAGE="https://haruna.kde.org/ https://apps.kde.org/haruna/"
SRC_URI="mirror://kde/stable/${PN}/${P}.tar.xz"

LICENSE="GPL-3+"
SLOT="6"
KEYWORDS="~amd64 ~arm64 ~ppc64 ~riscv ~x86"

RESTRICT="mirror"

BDEPEND="sys-devel/gettext"
DEPEND="
	!media-video/haruna:5
	>=dev-qt/qtbase-${QTMIN}:6[dbus,gui,network]
	>=dev-qt/qt5compat-${QTMIN}:6
	>=dev-qt/qtdeclarative-${QTMIN}:6[widgets]
	>=kde-frameworks/kcolorscheme-${KFMIN}:6
	>=kde-frameworks/kconfig-${KFMIN}:6
	>=kde-frameworks/kcoreaddons-${KFMIN}:6
	>=kde-frameworks/kfilemetadata-${KFMIN}:6[taglib]
	>=kde-frameworks/ki18n-${KFMIN}:6
	>=kde-frameworks/kiconthemes-${KFMIN}:6
	>=kde-frameworks/kio-${KFMIN}:6
	>=kde-frameworks/kirigami-${KFMIN}:6
	>=kde-frameworks/kwindowsystem-${KFMIN}:6
	kde-plasma/breeze:6
	media-libs/mpvqt
	media-video/ffmpeg
	media-video/mpv:=[libmpv]
"
RDEPEND="${DEPEND}
	net-misc/yt-dlp
"
