# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit qmake-utils

DESCRIPTION="A clone of Media Player Classic reimplemented in Qt"
HOMEPAGE="https://github.com/mpc-qt/mpc-qt"

SRC_URI="https://github.com/mpc-qt/mpc-qt/archive/v${PV}.tar.gz -> ${P}.tar.gz"
KEYWORDS="amd64 ~x86"
LICENSE="GPL-2"
SLOT="0"

DEPEND="media-video/mpv[libmpv]
	dev-qt/qtbase:6[dbus,gui,network,opengl,widgets]"
RDEPEND="${DEPEND}"

S=${WORKDIR}/${PN}-${PV}

src_configure() {
	eqmake6 PREFIX="${EPREFIX}/usr"
}

src_install() {
	emake install INSTALL_ROOT="${D}"
	mv "${ED}/usr/share/doc/${PN}" "${ED}/usr/share/doc/${P}" || die
}
