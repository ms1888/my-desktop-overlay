# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit ecm kde.org

DESCRIPTION="KDE extra colorschemes"
SRC_URI="https://download.kde.org/Attic/applications/15.08.3/src/kdeartwork-15.08.3.tar.xz"
KEYWORDS="~amd64 ~arm ~ppc ~ppc64 ~x86 ~amd64-linux ~x86-linux"
IUSE=""
SLOT="5"

PATCHES=( "${FILESDIR}/${P}-kf5-port.patch" )

S=${WORKDIR}/kdeartwork-${PV}

src_configure() {
	local mycmakeargs=(
		-DDISABLE_ALL_OPTIONAL_SUBDIRECTORIES=TRUE
		-DBUILD_ColorSchemes=TRUE
	)

	ecm_src_configure
}
