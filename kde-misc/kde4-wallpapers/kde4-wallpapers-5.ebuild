# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DESCRIPTION="KDE4 wallpapers"
HOMEPAGE="https://kde.org/"
SRC_URI="https://gitlab.com/ms1888/kde4-wallpapers/-/archive/${PV}/kde4-wallpapers-${PV}.tar"
KEYWORDS="amd64"
IUSE=""
SLOT="4"

RDEPEND="!kde-apps/kde-wallpapers"

src_install() {
	dodir /usr/share/wallpapers
	cp -r * "${ED}"/usr/share/wallpapers || die
}
