# Copyright 1999-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit qmake-utils xdg

DESCRIPTION="Desktop Independent Power Manager"
HOMEPAGE="https://github.com/rodlie/powerkit"
SRC_URI="https://github.com/rodlie/${PN}/releases/download/${PV}/${P}.tar.xz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND="dev-qt/qtcore:5
	dev-qt/qtdbus:5
	dev-qt/qtgui:5
	dev-qt/qtwidgets:5
	x11-libs/libX11
	x11-libs/libXrandr
	x11-libs/libXScrnSaver"
RDEPEND="${DEPEND}
	sys-apps/dbus
	sys-power/upower
	x11-misc/xscreensaver"

src_configure() {
	eqmake5 PREFIX="${EPREFIX}"/usr
}

src_install() {
	einstalldocs
	emake INSTALL_ROOT="${ED}" install
}
